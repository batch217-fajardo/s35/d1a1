const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 3001;

// [section] mongoDB connection
// mongoose.connect("SRV Link, {}", {useNewUrlParser: true, useUnifiedTopology: true })

mongoose.connect("mongodb+srv://admin:admin123@zuitt.w87b5cj.mongodb.net/b217_to-do?retryWrites=true&w=majority", {
	useNewUrlParser : true,
	useUnifiedTopology : true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database"));

// [section] mongoose schemas
// - determines the structure of our documents to be stored in the database

const taskSchema = new mongoose.Schema({
	name : {
		type: String,
		required: [true, "Task name is required"]
	},
	status : {
		type: String,
		default: "pending"
	}
});

const userSchema = new mongoose.Schema({
	username : {
		type: String,
		required: [true, "Username is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	}
});

// [section] models
const Task = mongoose.model("Task", taskSchema);

const User = mongoose.model("User", userSchema);

// [section] creation of to do list routes
app.use(express.json());
// allows your app to read data from forms
app.use(express.urlencoded({ extended: true }));

// [section] basic logic : creating a new task
/*
1. check if theere are duplicate tasks
- if the task already exist in th databas, we return an error
- if the task doesn't exist in the database, we add it in the database
2. the task data will be coming from the request's body.
3. create a new task object with a "name "
*/

app.post("/tasks", (req, res) => {
	Task.findOne({name: req.body.name}, (err, result) => {
		if(result != null && result.name == req.body.name){
			return res.send("Duplicate found");
		} else {
			let newTask = new Task({
				name: req.body.name
			});

			newTask.save((saveErr, saveTask) => {
				if(saveErr) {
					return console.error(saveErr);
				}else {
					return res.status(201).send("New task created");
				}
			})
		}
	})
})

// get method
app.get("/tasks", (req, res) => {
	Task.find({}, (err, result) => {
		if(err){
			return console.log(err);
		} else {
			return res.status(200).json({
				data: result
			})
		}
	})
})

app.post("/signup", (req, res) => {
	User.findOne({ username: req.body.username}, (err, result) => {
		if (result !== null && result.username == req.body.username){
			return res.send("User already exist.");
		} else {
			let newUser = new User ({
				username: req.body.username,
				password: req.body.password
			});

			newUser.save((saveErr, saveUser) => {
				if(saveErr) {
					return console.error(saveErr);
				}else {
					return res.status(201).send("New user registered.");
				}
			})
		}
	})
});


app.listen(port, () => console.log(`Server running at port ${port}.`));